package br.com.itau.investimento.controllers;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import br.com.itau.investimento.models.Aplicacao;
import br.com.itau.investimento.services.AplicacaoService;
import br.com.itau.investimento.valueobjects.Investimento;

@RestController
@RequestMapping("/aplicacao")
public class AplicacaoController {
	@Autowired
	private AplicacaoService aplicacaoService;
	
	@PostMapping
	@ResponseStatus(code = HttpStatus.CREATED)
	public Aplicacao criar(@RequestBody Investimento investimento){
		Optional<Investimento> investimentoOptional = aplicacaoService.criar(investimento);
		
		if(investimentoOptional.isPresent()) {
			return investimentoOptional.get().getAplicacao();
		}
		
		throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
	}
}
