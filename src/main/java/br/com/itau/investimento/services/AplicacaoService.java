package br.com.itau.investimento.services;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Optional;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.itau.investimento.models.Aplicacao;
import br.com.itau.investimento.models.Cliente;
import br.com.itau.investimento.models.Produto;
import br.com.itau.investimento.models.Transacao;
import br.com.itau.investimento.repositories.AplicacaoRepository;
import br.com.itau.investimento.repositories.TransacaoRepository;
import br.com.itau.investimento.valueobjects.Investimento;

@Service
public class AplicacaoService {
	@Autowired
	private AplicacaoRepository aplicacaoRepository;
	
	@Autowired
	private TransacaoRepository transacaoRepository;
	
	@Autowired
	private ClienteService clienteService;
	
	@Autowired
	private ProdutoService produtoService;
	
	public Optional<Investimento> criar(Investimento investimento) {
		Optional<Cliente> clienteOptional = clienteService.buscar(investimento.getCliente().getCpf());
		
		if(!clienteOptional.isPresent()) {
			return Optional.empty();
		}
		
		investimento.setCliente(clienteOptional.get());
	
		Optional<Produto> produtoOptional = produtoService.buscar(investimento.getProduto().getId());
		
		if(!produtoOptional.isPresent()) {
			return Optional.empty();
		}
		
		investimento.setProduto(produtoOptional.get());
		
		Aplicacao aplicacao = salvarAplicacao(investimento);
		investimento.setAplicacao(aplicacao);
		
		Transacao transacao = salvarTransacao(investimento);
		investimento.setTransacao(transacao);
		
		return Optional.of(investimento);
	}
		
	private Aplicacao salvarAplicacao(Investimento investimento) {
		Aplicacao aplicacao = new Aplicacao();
		
		aplicacao.setCliente(investimento.getCliente());
		aplicacao.setProduto(investimento.getProduto());
		aplicacao.setSaldo(investimento.getTransacao().getValor());
		aplicacao.setDataCriacao(LocalDate.now());
		
		return aplicacaoRepository.save(aplicacao);
	}
	
	private Transacao salvarTransacao(Investimento investimento) {
		Transacao transacao = investimento.getTransacao();
		
		transacao.setId(UUID.randomUUID());
		transacao.setAplicacao(investimento.getAplicacao());
		transacao.setTimestamp(LocalDateTime.now());
		
		return transacaoRepository.save(transacao);
	}
}
