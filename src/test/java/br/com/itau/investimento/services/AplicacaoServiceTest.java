package br.com.itau.investimento.services;

import static org.junit.Assert.assertNotNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

import java.util.Optional;
import java.util.UUID;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import br.com.itau.investimento.models.Aplicacao;
import br.com.itau.investimento.models.Cliente;
import br.com.itau.investimento.models.Produto;
import br.com.itau.investimento.models.Transacao;
import br.com.itau.investimento.repositories.AplicacaoRepository;
import br.com.itau.investimento.repositories.TransacaoRepository;
import br.com.itau.investimento.valueobjects.Investimento;

@RunWith(SpringRunner.class)
@ContextConfiguration(classes = AplicacaoService.class)
public class AplicacaoServiceTest {
    @Autowired
    private AplicacaoService aplicacaoService;
    @MockBean
    private AplicacaoRepository aplicacaoRepository;
    @MockBean
    private TransacaoRepository transacaoRepository;
    @MockBean
    private ClienteService clienteService;
    @MockBean
    private ProdutoService produtoService;

	private Investimento investimento;
	
	@Before
	public void preparar() {
  	    investimento = new Investimento();
        Transacao transacao = new Transacao();
        Cliente cliente = new Cliente();
        Produto produto = new Produto();
        
        cliente.setCpf("123.123.123-12");
        investimento.setCliente(cliente);
        
        transacao.setValor(1000);
        investimento.setTransacao(transacao);
        
        produto.setId(1);
        produto.setRendimento(0.005);
        investimento.setProduto(produto);
	}

	@Test
	public void testarCriarAplicacao() {
		definirRespostaBuscarCliente();
		definirRespostaBuscarProduto();
		definirRespostaCriarTransacao();
		definirRespostaCriarAplicacao();
		
		Investimento retorno = aplicacaoService.criar(investimento).get();

		assertNotNull(retorno.getAplicacao().getId());
		assertNotNull(retorno.getTransacao().getId());
	}
	
	private void definirRespostaBuscarCliente() {
		Cliente cliente = new Cliente();
		cliente.setCpf("123.123.123-12");
		
		when(clienteService.buscar(any(String.class)))
			.thenReturn(Optional.of(cliente));
	}
	
	private void definirRespostaBuscarProduto() {
		Produto produto = new Produto();
		produto.setId(1);
		
		when(produtoService.buscar(1))
			.thenReturn(Optional.of(produto));
		
	}
	
	private void definirRespostaCriarTransacao() {
		Transacao transacao = new Transacao();
		transacao.setId(UUID.randomUUID());
		transacao.setValor(1000);
		
		when(transacaoRepository.save(any(Transacao.class)))
				.thenReturn(transacao);
	}
	
	private void definirRespostaCriarAplicacao() {
		Aplicacao aplicacao = new Aplicacao();
		aplicacao.setId(1);
		
		when(aplicacaoRepository.save(any(Aplicacao.class)))
				.thenReturn(aplicacao);
	}
}
